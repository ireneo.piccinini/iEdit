var iEdit_Form = "iEdit_Form";
var iEdit_Area = "iEdit_Area";
var iEdit_Ref = "_iEdit_Ref";
var iEdit_Pure = {
	"26" : "&amp;",
	"E0" : "&agrave;",
	"E8" : "&egrave;",
	"E9" : "&eacute;",
	"EC" : "&igrave;",
	"F2" : "&ograve;",
	"F9" : "&ugrave;",
	"3C" : "&lt;",
}

var iE_purify = function(text) {
	var ret = text;
	for (ch in iEdit_Pure) {
		var re = new RegExp('\\x'+ch,'g');
		ret = ret.replace(re, iEdit_Pure[ch]);
	}
	return ret;
}

var iE_backspace = function(cont) {
	var area = document.getElementById(cont.id+iEdit_Ref);
	var text = cont.getElementsByTagName("div")[0];
	if (!area.value || !area.value.length) return;
	area.value = area.value.substring(0,area.value.length-1);
	// alert (iE_purify(text.lastChild.nodeValue));
	text.innerHTML = text.innerHTML.replace(/\n?(?:&[^;]+;|<[^>]+>|.)$/,'');
}

var iE_return = function(cont) {
	var area = document.getElementById(cont.id+iEdit_Ref);
	var text = cont.getElementsByTagName("div")[0];
	area.value += "\n";
	text.appendChild(document.createElement("br"));
}

var iE_form_submit = function(form) {
	if (form.hasFocus) return false;
	return true;
}

var iE_form_focus = function(into, state) {
	into.form.hasFocus = state;
}

var iEdit_init = function() {
	for (x in document.forms) {
		var form = document.forms[x];
		if (!form||!form.className||form.className.indexOf(iEdit_Form)<0) continue;
		for (y in form.elements) {
			var area = document.forms[x].elements[y];
			if (!area||area.nodeName!="TEXTAREA"||!area.className||area.className.indexOf(iEdit_Area)<0)
				continue;
			iE_area(form,area);
		}
	}
}

var iE_area = function(form,area) {
	form.onsubmit = function() { return iE_form_submit(this); }
	var into = '<input style="width:0;position:absolute;left:-9999px" onfocus="iE_form_focus(this,1)" onblur="iE_form_focus(this,0)" />';
	var text = '<div style="display:inline">' + iE_purify(area.value).replace("\n","\n<br />") + '</div>';
	var para = '<span class="iEdit_para">&para;</span>';
	var cont = document.createElement("div");
		cont.className = iEdit_Area;
		cont.id = area.id;
		cont.style.position = "relative";
		cont.style.cursor = "text";
		cont.style.overflow = "auto";
		cont.onmouseup = function() { this.onfocus(); }
		cont.onfocus = function() { iE_area_focus(this); }
		cont.onkeyup = function(event) { iE_input_letter(event,this); }
		cont.innerHTML = into + text + para;
	area.id += iEdit_Ref;
	//area.style.display = "none";
	area.parentNode.insertBefore(cont,area.nextSibling);
}

var iE_area_focus = function(area) {
	var into = area.getElementsByTagName("input")[0];
	into.focus();
}

var iE_input_letter = function(event,cont) {
	var into = cont.getElementsByTagName("input")[0];
	var text = cont.getElementsByTagName("div")[0];
	var area = document.getElementById(cont.id+iEdit_Ref);
	var key = 0;
	if (window.event) key = window.event.keyCode;
	else if (event) key = event.which;
	if (key == 8) iE_backspace(cont);
	else if (key == 13) iE_return(cont);
	else {
		area.value += into.value;
		text.appendChild(document.createTextNode(iE_purify(into.value)));
	}
	into.value = "";
	cont.scrollTop = cont.scrollHeight;
	iE_area_focus(cont);
	return false;
}

var iE_docReady = function() {
	iEdit_init();
}

if (window.addEventListener) window.addEventListener('load',iE_docReady,false);
else if (window.attachEvent) window.attachEvent('onload',iE_docReady);
else window.setTimeout('iE_docReady()',2000);